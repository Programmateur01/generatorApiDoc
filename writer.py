import json
import re
import sys

from var_dump import var_dump

class Default(dict):
    def __missing__(self, key):
        return key

class Writer:
    def __init__(self, files, mainFile):
        self.files = files
        self.mainFile = mainFile
        self.debug = 0

        self.mainJson = json.load(open(mainFile))

    def header(self, private = False):
        print('Generate header ...')

        f = open('./template/header.tp', 'r')
        html = f.read().format_map(Default(name=self.mainJson['name'], desc=self.mainJson['description']))
        html += open('./template/style.tp', 'r').read()
        f.close()

        print('Generate header - OK')
        return html
    def sidebar(self, private = False):
        print('Generate sidebar ...')
        html = '<ul class="sidenav nav nav-list"><li class="nav-fixed nav-header"><a href="#">Summary</a></li>\n'
        for i in range(0, len(self.files)):
            items = [];
            data = json.load(open(self.files[i]))
            if 'private' in data.keys() and data['private']:
                if not private:
                    print(' \033[1;31m' + data['name'] + '  -> Private\033[0m')
                else:
                    print(' {} (\033[1;31mP\033[0m)'.format(data['name']))
                    html += '<li class="nav-header"><a href="#{}">(P) {}</a></li>\n'.format(self.setLink(data['name']), data['name'])
                    for path in data['paths']:
                        items.append(path)
            else:
                print(' ' + data['name'])
                html += '<li class="nav-header"><a href="#{}">{}</a></li>\n'.format(self.setLink(data['name']), data['name'])
                for path in data['paths']:
                    items.append(path)
            html += self.menuItem(items, private)

        if 'objects' in self.mainJson.keys():
            print(' objects')
            html += '<li class="nav-fixed nav-header"><a href="#_objects">Objects</a></li>\n'
            for obj in self.mainJson['objects']:
                if 'private' in obj.keys() and obj['private']:
                    if not private:
                        print('      \033[1;31m' + obj['name'] + '  -> Private\033[0m')
                    else:
                        print('      {} (\033[1;31mP\033[0m)'.format(obj['name']))
                        html += '<li class="route"><a href="#{}">(P) {}</a></li>\n'.format(self.setLink('obj_' + obj['name']), obj['name'])
                else:
                    print('      ' + obj['name'])
                    html += '<li class="route"><a href="#{}">{}</a></li>\n'.format(self.setLink('obj_' + obj['name']), obj['name'])

        html += "</ul>\n"
        print('Generate sidebar - OK')
        return html
    def section(self, private = False):
        print('Generate section ...')
        html = ''
        for i in range(0, len(self.files)):
            data = json.load(open(self.files[i]))

            if 'private' in data.keys() and data['private']:
                if not private:
                    print(' \033[1;31m' + data['name'] + '  -> Private\033[0m')
                else:
                    print(' {} (\033[1;31mP\033[0m)'.format(data['name']))
                    html += '<section id="{}"><h1>(P) {}</h1>\n'.format(self.setLink(data['name']), data['name'])
                    for path in data['paths']:
                        html += self.article(path, private)
                    html += '</section>\n'
            else:
                print(' ' + data['name'])
                html += '<section id="{}"><h1>{}</h1>\n'.format(self.setLink(data['name']), data['name'])
                for path in data['paths']:
                    html += self.article(path, private)
                html += '</section>\n'

        print('Generate section - OK')
        return html
    def footer(self, private = False):
        print('Generate footer ...')
        html = ''
        if 'objects' in  self.mainJson.keys():
            html += '<section id="_objects"><h1>Objects</h1>\n'
            self.mainJson['objects'] = sorted(self.mainJson['objects'], key=lambda k: k['name'])
            for obj in sorted(self.mainJson['objects'], key=lambda k: k['name']):
                if 'private' in obj.keys() and obj['private']:
                    if not private:
                        print('  \033[1;31m' + obj['name'] + '  -> Private\033[0m')
                    else:
                        print('  {} (\033[1;31mP\033[0m)'.format(obj['name']))
                        html += '<div id="{}"><article>\n'.format('obj_' + self.setLink(obj['name']))
                        html += '<div class="pull-left"><h1>(P) {}</h1><p>{}</p></div>\n'.format(obj['name'], (obj['description'] if 'description' in obj.keys() else ''))
                        html += '<div class="pull-right"></div><div class="clearfix"></div>\n'
                        html += self.object(obj)
                        html += '</div></article>\n'
                else:
                    print('  {}'.format(obj['name']))
                    html += '<div id="{}"><article>\n'.format('obj_' + self.setLink(obj['name']))
                    html += '<div class="pull-left"><h1>{}</h1><p>{}</p></div>\n'.format(obj['name'], (obj['description'] if 'description' in obj.keys() else ''))
                    html += '<div class="pull-right"></div><div class="clearfix"></div>\n'
                    html += self.object(obj)
                    html += '</div></article>\n'
            html += '</section>\n'

        f = open('./template/footer.tp', 'r')
        html += f.read()
        f.close()

        print('Generate footer - OK')
        return html

    '''
    Fonctions secondaires
    '''
    def array(self, array):
        html = '<div class="code-tab-content">\n'
        html += '<span class="toggle-handle"></span><span class="opening brace"> [ </span>\n<div class="inner">\n'
        for val in array['items']:
            html += '<div>\n'
            ref = re.search(r'\$ref:(.*)', val)
            if ref:
                 html += '<span class="name"><a href="#{}">{}</a></span>\n'.format(self.setLink('obj_' + ref.group(1)), ref.group(1))
            else:
                html += '<span class="name">{}</a></span>\n'.format(val)
            html += '</div>\n'
        html += '</div>\n<span class="closing brace">]</span>\n</div>\n'
        return html
        return html
    def article(self, path, private = False):
        html = ''
        print('      /' + path['route'])
        for method in path.keys():
            if method != 'get' and method != 'post' and method != 'put':
                continue
            if 'private' in path[method].keys() and path[method]['private']:
                if private:
                    print('       {} {} (\033[1;31mP\033[0m)'.format(method, path[method]['name']))
                    name = '(P) {}'.format(path[method]['name'])
                else:
                    print('       \033[1;31m{} {}  -> Private\033[0m'.format(method, path[method]['name']))
                    continue
            else:
                print('       {} {}'.format(method, path[method]['name']))
                name = '{}'.format(path[method]['name'])
            f = open('./template/article_head.tp', 'r')
            html += f.read().format_map(Default(id=self.setLink(path[method]['name']), name=name, summary=path[method]['summary'], method=method, path=path['route'], description=(path[method]['description'] if 'description' in path[method].keys() else '')))
            f.close()
            if 'parameters' in path[method].keys():
                # We got parameters
                html += '<h2>Parameters</h2>\n';
                # parse parameters
                p = {'header': [], 'body': [], 'path': []}
                for param in path[method]['parameters']:
                    for m in p:
                        if m == param['in']:
                            p[m].append(param)

                for m in p:
                    if len(p[m]) > 0:
                        html += '<div class="methodsubtabletitle">{} parametres</div><table id="methodsubtable">\n'.format(m.capitalize())
                        html += '<tbody><tr><th width="150px">Name</th><th>Description</th></tr>'
                        for param in p[m]:
                            html += self.param(param)
                        html += '</tbody></table>\n'

            if 'responses' in path[method].keys():
                html += "<h2>Responses</h2>\n"
                for res in path[method]['responses']:
                    html += '<h3> Status: {} - {}</h3>\n<ul class="nav nav-tabs nav-tabs-examples"></ul><div class="tab-content" style="margin-bottom: 10px;"></div>'.format(res['code'], res['description'])
                    if 'schema' in res and '$ref' not in res['schema']:
                        if res['schema']['type'] == 'object':
                            html += self.object(res['schema'])
                        elif res['schema']['type'] == 'array':
                            html += self.array(res['schema'])
            html += '</article></div>\n'
        return html

    def menuItem(self, paths, private = False):
        html = ''
        items = []
        for path in paths:
            for m in ['get', 'post', 'put']:
                if m in path.keys():
                    if 'private' in path[m].keys() and path[m]['private']:
                        if private:
                            # items.append('(P) {}'.format(path[m]['name']))
                            items.append([path[m]['name'], '(P) {}'.format(path[m]['name'])])
                            # html += '<li class="route"><a href="#{}">(P) {}</a></li>\n'.format(self.setLink(path[m]['name']), path[m]['name'])
                    else:
                        items.append([path[m]['name'], path[m]['name']])

        # write
        for item in sorted(items, key=lambda k: k[0]):
            html += '<li class="route"><a href="#{}">{}</a></li>\n'.format(self.setLink(item[1]), item[0])

        return html
    def object(self, obj):
        html = '<div class="code-tab-content">\n'
        html += '<span class="toggle-handle"></span><span class="opening brace"> { </span>\n<div class="inner">\n'
        for prop in sorted(obj['properties']):
            html += '<div>\n'
            html += '<span class="type">{}</span>\n'.format(obj['properties'][prop]['type'])
            html += '<span class="name">{}</span>\n'.format(prop)
            html += '<span>{}</span>\n'.format((obj['properties'][prop]['description'] if 'description' in obj['properties'][prop].keys() else ''))
            html += '</div>\n'
        html += '</div>\n<span class="closing brace">}</span>\n</div>\n'
        return html
    def param(self, param):
        html = ''
        f = open('./template/param.tp', 'r')
        if '$ref' in param.keys():
            param['type'] = '<a href="#{}">{}</a>'.format(self.setLink('obj_' + param ['$ref']), param ['$ref'])
        if 'required' in param.keys() and param['required']:
            html += f.read().format_map(Default(name=(param['name'] if 'name' in param.keys() else ''), desc=(param['description'] if 'description' in param.keys() else ''), type=(param['type'] if 'type' in param.keys() else ''), require='*', required='obligatoire'))
        else :
            html += f.read().format_map(Default(name=(param['name'] if 'name' in param.keys() else ''), desc=(param['description'] if 'description' in param.keys() else ''), type=(param['type'] if 'type' in param.keys() else ''), require='', required=''))
        f.close()
        return html
    def setLink(self, str):
        return str.lower().replace(' ', '_')

    def write(self, output, private = False):
        html = ''

        html += self.header(private)
        html += self.sidebar(private)
        html += self.section(private)
        html += self.footer(private)

        f = open(output, 'w+')
        f.write(html)
        f.close()
