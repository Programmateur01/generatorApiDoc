#!/usr/bin/env python3

import argparse
import json
import os
import re
import sys

from writer import Writer

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Super generateur de doc !");
    parser.add_argument("-i", "--input", help = "Localisation du dossier contenant les json")
    parser.add_argument("-o", "--output", help = "Localisation du fichier html sortant")
    parser.add_argument("-p", "--private", action="store_true", help = "Write private documentation")
    args = parser.parse_args()
    # print(args.input)

    try :
        allFiles = os.listdir(args.input)
        jsonFiles = []
        mainFile = ''
        # print(allFiles)
        for i in range(0, len(allFiles)):
            # control to get only json files
            if (re.match(r'.*\.json$', allFiles[i])):
                # print(allFiles[i])
                if (re.match(r'__main__\.json$', allFiles[i])):
                    #find main file
                    mainFile = os.path.join(args.input, allFiles[i])
                else:
                    jsonFiles.append(os.path.join(args.input, allFiles[i]))

    except:
        print('Fail to load files')
        e = sys.exc_info()[0] # get error
        print("Error: {}".format(e))
        sys.exit(0)

    writer = Writer(jsonFiles, mainFile)
    writer.write(args.output, args.private)
