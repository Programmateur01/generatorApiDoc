.PHONY : all private public

RM=rm -rvf

all: private public

private:
	./main.py -i ../json/ -o docPrivate.html -p

public:
	./main.py -i ../json/ -o docPublique.html

clean:
	@$(RM) docPrivate.html
	@$(RM) docPublique.html
